import sys
import string
import json
from itertools import chain, repeat, islice
import itertools
from collections import Counter
import random

def keyCheck(key, dic):
    if key in dic.keys():
        return "initialised"
    else:
        return "notInitialised"

def intersperse(delimiter, seq):
    return islice(chain.from_iterable(zip(repeat(delimiter), seq)), 1, None)

def getClass(a):
	i=0 
	while a[i]!=' ':
		i=i+1
	clas=a[0:i] 
	return clas

def trainingFunc(uniqueWords,myInput,mainDict,numLines,listClass):
	lineNum=0
	flag=0
	newDict=mainDict.copy()
	mainDict1={}	
	
	lines = myInput.read().splitlines()
	random.shuffle(lines)
	for line in lines:
		#newDict=mainDict.copy()
		#print("newLine")
		frequency={}
		lineNum+=1
		#print(numLines)
		line=line.strip(" \n")
		line=line.strip("\n")
		features=line.split(' ')
		
		
		features2=features.copy()
		
		del(features2[0])
		for a in range(0,len(features2)):
			features2[a]=features2[a].lower()
		#print(features2)
		for word in features2:
			if keyCheck(word, frequency)=="notInitialised":
				frequency[word]=1
			else:
				frequency[word]+=1
	

					
		DictClassSum={}
		listSums=[]
		for key in listClass:
			s=0
			for word in features2:
				s=s+(frequency[word]*mainDict[key][word])
			DictClassSum[key]=s
			listSums.append(s)
		for key1,value in DictClassSum.items():
			if value==max(listSums):
		
				predictedClass=key1
				break
		
		
		if predictedClass!=features[0]:
			#print("wrong prediction")
			flag=1
	
			for word in features2:
			
				mainDict[predictedClass][word]-=frequency[word]
				mainDict[features[0]][word]+=frequency[word]

	#newDict=mainDict.copy()
	for clas in listClass:
		
		for word in uniqueWords:
			newDict[clas][word]=mainDict[clas][word]+newDict[clas][word]	
			
	return 1,mainDict

	

        
def createInitialDict(listClass,myInput,mainDict):
	innerDict={}
	features1=[]
	numLines=0
	features2=[]
	features3=[]
	for line in myInput:
		
		numLines+=1
		#print(numLines)
		line=line.strip(" \n")
		line=line.strip("\n")
		features=line.split(' ')
		clas=features[0]
		if clas not in listClass:
			listClass.append(clas)

		features1=features.copy()
		del(features1[0])
		features2+=features1
	for i in range(0,len(features2)):
		features2[i]=features2[i].lower()
	features2 = list(set(features2))
	#print(features2)
	features3=features2.copy()
	#print(features3)
	features2=intersperse(0,features2)
	d = dict(itertools.zip_longest(*[iter(features2)]*2, fillvalue=0))
	#print (d)
	for clas in listClass:
		d_copy=d.copy()
		mainDict[clas]=d_copy
		d_copy={}
	return features3,listClass,numLines,mainDict

	
def checkAccuracy(myInput,inputDev,mainDict,accuracy,listClass):
	lineNum=0
	flag=0
	newDict=mainDict.copy()
	mainDict1={}
	correctPredictions=0	
	lines = inputDev.read().splitlines()
	random.shuffle(lines)	
	for line in lines:
		newDict=mainDict.copy()
		#print("newLine")
		frequency={}
		lineNum+=1
		#print(lineNum)
		#print(numLines)
		line=line.strip(" \n")
		line=line.strip("\n")
		features=line.split(' ')
		
		
		features2=features.copy()
		
		del(features2[0])
		for a in range(0,len(features2)):
			features2[a]=features2[a].lower()
		#print(features2)
		for word in features2:
			if keyCheck(word, frequency)=="notInitialised":
				frequency[word]=1
			else:
				frequency[word]+=1
	

					
		DictClassSum={}
		listSums=[]
		for key in listClass:
			s=0
			for word in features2:
				if keyCheck(word,mainDict[key])=="notInitialised":
					continue
				else:
					s=s+(frequency[word]*mainDict[key][word])
			
			DictClassSum[key]=s
			listSums.append(s)
		for key1,value in DictClassSum.items():
			if value==max(listSums):
		
				predictedClass=key1
				break
		
		if predictedClass==features[0]:
		
			correctPredictions+=1
	print("correctPredictions:"+str(correctPredictions))
	acc=correctPredictions/lineNum
	print (acc)
	if acc>accuracy:
		return acc,1
	else:
		return accuracy,0
		
def funct(a,b,c,d):
	
	myInput=open(a,"r")
	mainDict={}
	finalMainDict={}
	listClass=[]
	flag=1
	uniqueWords=[]
	uniqueWords,listClass,numLines,mainDict=createInitialDict(listClass,myInput,mainDict)

	print("end of initialisation")
	num_iterations=0
	accuracy=0
	while num_iterations<20:
		myInput=open(a,"r")
		num_iterations+=1
		print("iteration num:"+str(num_iterations))	
		flag,mainDict=trainingFunc(uniqueWords,myInput,mainDict,numLines,listClass)

		myInput=open(a,"r")
		if len(sys.argv)>3:
			inputDev=open(d,"r")
			print("checking on dev:")
			accuracy,returnVal=checkAccuracy(myInput,inputDev,mainDict,accuracy,listClass)
			if returnVal==1:
				print("accuracy greater")
				finalMainDict=mainDict
	
	
	finalMainDict=mainDict
	jsonData={}
	jsonData['mainDictionary']=	finalMainDict
	jsonData['listClas']=listClass
	with open(b,"w+") as out_file:
	  json.dump(jsonData,out_file,indent=4)
	

def main():
	
	myInput=open(sys.argv[1],"r")
	mainDict={}
	finalMainDict={}
	listClass=[]
	flag=1
	uniqueWords=[]
	uniqueWords,listClass,numLines,mainDict=createInitialDict(listClass,myInput,mainDict)

	print("end of initialisation")
	num_iterations=0
	accuracy=0
	while num_iterations<20:
		myInput=open(sys.argv[1],"r")
		num_iterations+=1
		print("iteration num:"+str(num_iterations))	
		flag,mainDict=trainingFunc(uniqueWords,myInput,mainDict,numLines,listClass)

		myInput=open(sys.argv[1],"r")
		if len(sys.argv)>3:
			inputDev=open(sys.argv[4],"r")
			print("checking on dev:")
			accuracy,returnVal=checkAccuracy(myInput,inputDev,mainDict,accuracy,listClass)
			if returnVal==1:
				print("accuracy greater")
				finalMainDict=mainDict
	
	
	finalMainDict=mainDict
	jsonData={}
	jsonData['mainDictionary']=	finalMainDict
	jsonData['listClas']=listClass
	with open(sys.argv[2],"w+") as out_file:
	  json.dump(jsonData,out_file,indent=4)

if __name__=="__main__":
	main()

						
