What is the accuracy of your part-of-speech tagger?
-Accuracy of part-of-speech tagger is 92.98%

What are the precision, recall and F-score for each of the named entity types for your named entity recognizer, and what is the overall F-score?

LOC  precision=0.385265932   recall=0.214739458 F-score=0.276216627

ORG  precision=0.325769484 recall=0.3856795860  F-score=0.3532402608

PER  precision=0.47205094  recall=0.447948234  F-score=0.4596843449

MISC  precision=0.283759643  recall=0.22568957  F-score=0.2514200594

What happens if you use your Naive Bayes classifier instead of your perceptron classifier (report performance metrics)? Why do you think that is?

-If Naive BAyes CLassifier is used,the accuracy is slightly lower.

Consider the spam and ham dataset
Using Naive Bayes,these were the results

HAM
precision:0.9889558232931727
recall:0.985
F-score:0.9869739478957915

SPAM
precision:0.9591280653950953
recall:0.9696969696969697
F-score:0.9643835616438355

Using Perceptron,these were the results
HAM
precision:0.978
recall:0.9655656745868356835
F-score:0.9717098019

SPAM
precision:0.984578973453459
recall:0.95756786985753421
F-score:0.9708857416

In POS tagging,perceptron gave an accuracy of 92.98% on development data whereas the NB classifier gave only a 78% accuracy.


Reason:-
In POS tagging using perceptron,we took the context into consideration(previous and next word)due to which the accuracy was better than NB classifier which took words individually without taking advantage of the order of words and its relative context in the sentence. 
Perceptron makes lesser assumptions about data whereas the Naive Bayes classifier assumes all features(bag of words) to be independent.
Perceptron makes multiple passes through data due to which there is mistake driven learning which results in better performance due to updation of weights in each iteration.
The average time taken  by perceptron was observed to be higher possibly due to the multiple iterations through data which is not the case in NB classifier due to which the classification was on an average faster in NB.
