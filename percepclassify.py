import json
import sys
import codecs

def keyCheck(key, dic):
    if key in dic.keys():
        return "initialised"
    else:
        return "notInitialised"
	
def classify(mainDict,listClass):
	lineNum=0
	flag=0
	newDict=mainDict.copy()
	mainDict1={}
	correctPredictions=0	
	#output=open(sys.argv[3],"w")
	
	inputLines=codecs.getreader('latin-1')(sys.stdin.detach(), errors='ignore')
	for line in inputLines:
		
		newDict=mainDict.copy()
		#print("newLine")
		frequency={}
		lineNum+=1
		#print(lineNum)
		#print(numLines)
		line=line.strip(" \n")
		line=line.strip("\n")
		features=line.split(' ')
		
		
		features2=features.copy()
		
		
		for a in range(0,len(features2)):
			features2[a]=features2[a].lower()
		#print(features2)
		for word in features2:
			if keyCheck(word, frequency)=="notInitialised":
				frequency[word]=1
			else:
				frequency[word]+=1
	

					
		DictClassSum={}
		listSums=[]
		for key in listClass:
			s=0
			for word in features2:
				if keyCheck(word,mainDict[key])=="notInitialised":
					continue
				else:
					s=s+(frequency[word]*mainDict[key][word])
			
			DictClassSum[key]=s
			listSums.append(s)
		for key1,value in DictClassSum.items():
			if value==max(listSums):
				
				predictedClass=key1
			
				
				print(predictedClass)
				
				
				break

def main():		
	mainDict={}


	with open(sys.argv[1]) as json_file:
		mainDict1 = json.load(json_file)
	for key,value in mainDict1.items():
		if key=="mainDictionary":
			mainDict=value
		if key=="listClas":
			listClass=value

	#print(mainDict)
	#myInput=open(sys.argv[2],"r")

	classify(mainDict,listClass)

if __name__=="__main__":
	main()
		
