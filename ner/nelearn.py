import sys
import string
import re
import perceplearn

def triplet(myInput,myOutput):
	#myOutput=open(sys.argv[2],"w")  #file in which triplets are written
	for line in myInput:
		
		listWord=[]
		myList=[]
		listPOS=[]
		listNER=[]
		line=line.strip(" \n")
		line=line.strip("\n")
		features=line.split(' ')

		for i in features:
			pos=i.rindex('/')
			listNER.append(i[pos+1:])
			w=i[0:pos]
			pos=w.rindex('/')
			listPOS.append(w[pos+1:])
			listWord.append(w[0:pos])
		#print(listWord)
		#print(listPOS)
		#print(listNER)
		
		
		for a in range(0,len(listWord)):
			l={}
			l2={}
			l3={}
			if a==0:
				l["prev"]="BOS"
				l2["prev"]="BOS"
				l3["prev"]="BOS"
			else:
				l["prev"]=listWord[a-1].lower()
				l2["prev"]=listPOS[a-1]
				l3["prev"]=listNER[a-1]
			l["curr"]=listWord[a].lower()
			l2["curr"]=listPOS[a]
			if a==len(listWord)-1:
				l["next"]="EOS"
				l2["next"]="EOS"
			else:
				l["next"]=listWord[a+1].lower()
				l2["next"]=listPOS[a+1]
			tagForTriplet=listNER[a]
			#print(listWord)
			#myLine=tagForTriplet+" prev:"+l["prev"]+",prev:"+l2["prev"]+",prev:"+l3["prev"]+" curr:"+l["curr"]+",curr:"+l2["curr"]+" next:"+l["next"]+",next:"+l2["next"]+"\n"
			#print(myLine)	
			#myLine=tagForTriplet+" prev:"+l["prev"]+" curr:"+l["curr"]+" next:"+l["next"]+"\n"
			myLine=tagForTriplet+" prev:"+l["prev"]+",prev:"+l3["prev"]+" curr:"+l["curr"]+" next:"+l["next"]+"\n"	
			myOutput.write(myLine)

						
			a+=1			
	       

def main():
	myInput=open(sys.argv[1],"r",errors="ignore")   #ner.esp.train file
	myOutput=open("trainTriplet","w")  #file in which triplets are written
	triplet(myInput,myOutput)
	output1=open("devTriplet","w")
	if len(sys.argv)>3:
		input1=open(sys.argv[4],"r",errors="ignore")
		triplet(input1,output1)
		perceplearn.functNER("trainTriplet",sys.argv[2],sys.argv[3],"devTriplet")
	else:
	
		perceplearn.functNER("trainTriplet",sys.argv[2],0,0)
	

if __name__=="__main__":
	main()
