"""
		Anne Mary Joy
		USC ID:2349496852

"""
import sys
import string
import percepclassify
import io
import codecs
import json

def keyCheck(key, dic):
    if key in dic.keys():
        return "initialised"
    else:
        return "notInitialised"

def classify(mainDict,file_name,listClass,output,flag):
	lineNum=0
	flag=0
	newDict=mainDict.copy()
	mainDict1={}
	correctPredictions=0	
	#output=open(sys.argv[3],"w")
	#myInput=open(file_name,"r")
	listInput=[]
	listOutputTags=[]
	inputLines=codecs.getreader('latin-1')(sys.stdin.detach(), errors='ignore')
	for line in inputLines:
		f=0
		lineNum+=1
		listWord=[]
		listPOS=[]		
		line=line.strip(" \n")
		line=line.strip("\n")
		listInput.append(line)
		features=line.split(' ')
		
		for i in features:
			
			listWord.append(i.split('/', 1)[0])
		#print(features)		
		for i in features:
			
			listPOS.append(i.split('/', 1)[1])
		
		for a in range(0,len(features)):
			l={}
			l2={}
			l3={}
			if a==0:
				l["prev"]="BOS"
				l2["prev"]="BOS"
				
				l3["prev"]="BOS"
			else:
				l["prev"]=listWord[a-1].lower()
				l2["prev"]=listPOS[a-1]
				if f==0:
					l3["prev"]="BOS"
					f=1
				else:
					l3["prev"]=st
			l["curr"]=listWord[a].lower()
			l2["curr"]=listPOS[a]
			
			if a==len(features)-1:
				l["next"]="EOS"
				l2["next"]="EOS"
				
			else:
				
				l["next"]=listWord[a+1].lower()
				l2["next"]=listPOS[a+1]
			
			
			myLine="prev:"+l["prev"]+" prev:"+l2["prev"]+" prev:"+l3["prev"]+" curr:"+l["curr"]+" curr:"+l2["curr"]+" next:"+l["next"]+" next:"+l2["next"]
			#myLine="prev:"+l["prev"]+",prev:"+l3["prev"]+" curr:"+l["curr"]+" next:"+l["next"]
			
			newDict=mainDict.copy()
			#print("newLine")
			frequency={}
			lineNum+=1
			#print(lineNum)
			#print(numLines)
			#myLine=myLine.strip(" \n")
			#myLine=myLine.strip("\n")
			feature=myLine.split(' ')
		
		
			features2=feature.copy()
		
		
		
			#print(features2)
			for word in features2:
				if keyCheck(word, frequency)=="notInitialised":
					frequency[word]=1
				else:
					frequency[word]+=1
	

					
			DictClassSum={}
			listSums=[]
			for key in listClass:
				s=0
				for word in features2:
					if keyCheck(word,mainDict[key])=="notInitialised":
						continue
					else:
						s=s+(frequency[word]*mainDict[key][word])
			
				DictClassSum[key]=s
				listSums.append(s)
			for key1,value in DictClassSum.items():
				if value==max(listSums):
				
					predictedClass=key1
					listOutputTags.append(predictedClass)
				
					if flag==1:
						#print(predictedClass+"\n")
						st=predictedClass
					if flag==0:
						#st=features2[1]
						#st=st.split(":",1)[1]
						#sys.stdout.write(st+"/")
					
						#print(predictedClass+" ")
						st=predictedClass
				
					break
	v=-1
		
	for k in listInput:
		
		k=k.strip(" \n")
		k=k.strip("\n")
		featr=k.split(' ')
		for aa in featr:
			
			sys.stdout.write(aa)
			sys.stdout.write("/")
			v+=1
			sys.stdout.write(listOutputTags[v])
			sys.stdout.write(" ")
		sys.stdout.write("\n")
		

def main():
	#myInput=open(sys.argv[1],"r")   #ner.esp.blind.test file to be taken by stdin
	
	
	
	#triplet()
	#u=open("tripletTestPOS.txt","r")
	#print(u.read())
	#lineU=0
	#for line in u:
	#	lineU+=1
	#print(lineU)
	#functNER(sys.argv[1],"tripletTestPOS.txt")
	mainDict={}
	

	with open(sys.argv[1]) as json_file:
		mainDict1 = json.load(json_file)
	for key,value in mainDict1.items():
		if key=="mainDictionary":
			mainDict=value
		if key=="listClas":
			listClass=value

	#print(mainDict)
	myInput=open("tripletTestPOS.txt","w")
	#output=open(c,"w")
	#lineU=0
	#for line in myInput:
	#	lineU+=1
	#print(lineU)
	classify(mainDict,"tripletTestPOS.txt",listClass,0,0)

if __name__=="__main__":
	main()

"""
def main():
	#myInput=open(sys.argv[1],"r")   #ner.esp.blind.test file to be taken by stdin
	
	
	
	triplet()
	u=open("tripletTestPOS.txt","r")
	#print(u.read())
	lineU=0
	for line in u:
		lineU+=1
	print(lineU)
	#functNER(sys.argv[1],"tripletTestPOS.txt")
	mainDict={}
	

	with open(sys.argv[1]) as json_file:
		mainDict1 = json.load(json_file)
	for key,value in mainDict1.items():
		if key=="mainDictionary":
			mainDict=value
		if key=="listClas":
			listClass=value

	#print(mainDict)
	myInput=open("tripletTestPOS.txt","r")
	#output=open(c,"w")
	
	classify(mainDict,"tripletTestPOS.txt",listClass,0,0)

def triplet():
	#sys.stdin=  codecs.getreader(sys.stdin.encoding)(sys.stdin)
	#sys.stdin=codecs.getreader('utf8')(sys.stdin.detach())
	inputLines=codecs.getreader('latin-1')(sys.stdin.detach(), errors='ignore')
	myOutput=open("tripletTestPOS.txt","w")  #file in which triplets are written
	for line in inputLines: 
		
		listWord=[]
		listPOS=[]		
		line=line.strip(" \n")
		line=line.strip("\n")
		features=line.split(' ')
		
		for i in features:
			
			listWord.append(i.split('/', 1)[0])
		#print(features)		
		for i in features:
			
			listPOS.append(i.split('/', 1)[1])
		
		for a in range(0,len(features)):
			l={}
			l2={}
			if a==0:
				l["prev"]="BOS"
				l2["prev"]="BOS"
			else:
				l["prev"]=listWord[a-1].lower()
				l2["prev"]=listPOS[a-1]
			l["curr"]=listWord[a].lower()
			l2["curr"]=listPOS[a]
			if a==len(features)-1:
				l["next"]="EOS"
				l2["next"]="EOS"
			else:
				l["next"]=listWord[a+1].lower()
				l2["next"]=listPOS[a+1]
			
			
			myLine="prev:"+l["prev"]+",prev:"+l2["prev"]+" curr:"+l["curr"]+",curr:"+l2["curr"]+" next:"+l["next"]+",next:"+l2["next"]+"\n"
					
			myOutput.write(myLine)
			
			a+=1


def classify(mainDict,file_name,listClass,output,flag):
	lineNum=0
	flag=0
	newDict=mainDict.copy()
	mainDict1={}
	correctPredictions=0	
	#output=open(sys.argv[3],"w")
	myInput=open(file_name,"r")
	
	for line in myInput:
		
		newDict=mainDict.copy()
		#print("newLine")
		frequency={}
		lineNum+=1
		#print(lineNum)
		#print(numLines)
		line=line.strip(" \n")
		line=line.strip("\n")
		features=line.split(' ')
		
		
		features2=features.copy()
		
		
		#print(features2)
		for word in features2:
			if keyCheck(word, frequency)=="notInitialised":
				frequency[word]=1
			else:
				frequency[word]+=1
	

					
		DictClassSum={}
		listSums=[]
		for key in listClass:
			s=0
			for word in features2:
				if keyCheck(word,mainDict[key])=="notInitialised":
					continue
				else:
					s=s+(frequency[word]*mainDict[key][word])
			
			DictClassSum[key]=s
			listSums.append(s)
		for key1,value in DictClassSum.items():
			if value==max(listSums):
				
				predictedClass=key1
				
				if flag==1:
					print(predictedClass+"\n")
				if flag==0:
					#st=features2[1]
					#st=st.split(":",1)[1]
					#sys.stdout.write(st+"/")
					
					print(predictedClass+" ")
				
				break
	print(lineNum)	
"""	

