import json
import sys

def keyCheck(key, dic):
    if key in dic.keys():
        return "initialised"
    else:
        return "notInitialised"
	
def classify(mainDict,b,listClass,output,flag,listInput):
	lineNum=0
	flag=0
	newDict=mainDict.copy()
	mainDict1={}
	correctPredictions=0	
	#output=open(sys.argv[3],"w")
	listOutputTags=[]

	
	myInput=open(b,"r")
	for line in myInput:
		
		newDict=mainDict.copy()
		#print("newLine")
		frequency={}
		lineNum+=1
		#print(lineNum)
		#print(numLines)
		line=line.strip(" \n")
		line=line.strip("\n")
		features=line.split(' ')
		
		
		features2=features.copy()
		
		
		for a in range(0,len(features2)):
			features2[a]=features2[a].lower()
		#print(features2)
		for word in features2:
			if keyCheck(word, frequency)=="notInitialised":
				frequency[word]=1
			else:
				frequency[word]+=1
	

					
		DictClassSum={}
		listSums=[]
		for key in listClass:
			s=0
			for word in features2:
				if keyCheck(word,mainDict[key])=="notInitialised":
					continue
				else:
					s=s+(frequency[word]*mainDict[key][word])
			
			DictClassSum[key]=s
			listSums.append(s)
		for key1,value in DictClassSum.items():
			if value==max(listSums):
				
				predictedClass=key1
				
				if flag==1:
					output.write(predictedClass+"\n")
				if flag==0:
					#st=features2[1]
					#st=st.split(":",1)[1]
					#sys.stdout.write(st)
					#print(predictedClass)
					listOutputTags.append(predictedClass)
				break
	
	
	v=-1
		
	for k in listInput:
		
		k=k.strip(" \n")
		k=k.strip("\n")
		featr=k.split(' ')
		for aa in featr:
			
			sys.stdout.write(aa)
			sys.stdout.write("/")
			v+=1
			sys.stdout.write(listOutputTags[v])
			sys.stdout.write(" ")
		sys.stdout.write("\n")
				 
	
def funct(a,b,listInput):		
	mainDict={}


	with open(a) as json_file:
		mainDict1 = json.load(json_file)
	for key,value in mainDict1.items():
		if key=="mainDictionary":
			mainDict=value
		if key=="listClas":
			listClass=value

	#print(mainDict)
	#myInput=open(b,"r")
	#output=open(c,"w")
	classify(mainDict,b,listClass,0,0,listInput)

def main():		
	mainDict={}


	with open(sys.argv[1]) as json_file:
		mainDict1 = json.load(json_file)
	for key,value in mainDict1.items():
		if key=="mainDictionary":
			mainDict=value
		if key=="listClas":
			listClass=value

	#print(mainDict)
	myInput=open(sys.argv[2],"r")
	output=open(sys.argv[3],"w")
	classify(mainDict,myInput,listClass,output,1,listInput)

if __name__=="__main__":
	main()
		
