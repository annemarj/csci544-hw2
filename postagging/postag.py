"""
		Anne Mary Joy
		USC ID:2349496852

"""
import sys
import string
import percepclassify

def triplet(a):
	myOutput=open(a,"w")
	listInput=[]
	for line in sys.stdin: 
	
		
		line=line.strip(" \n")
		line=line.strip("\n")
		listInput.append(line)
		features=line.split(' ')

		
		for a in range(0,len(features)):
			l={}
			if a==0:
				l["prev"]="BOS"
			else:
				l["prev"]=features[a-1].lower()
			l["curr"]=features[a].lower()
			if a==len(features)-1:
				l["next"]="EOS"
			else:
				l["next"]=features[a+1].lower()
			
			
			myLine="prev:"+l["prev"]+" curr:"+l["curr"]+" next:"+l["next"]+"\n"
					
			myOutput.write(myLine)
			
			a+=1
	
	return listInput
				
	
def main():
	
	#myInput=open(sys.argv[1],"r")   #pos.blind.test file to be taken by stdin
	#myOutput=open("tripletTestPOS","w")  #file in which triplets are written
	listInput=triplet("tripletTestPOS")
	percepclassify.funct(sys.argv[1],"tripletTestPOS",listInput)

if __name__=="__main__":
	main()
