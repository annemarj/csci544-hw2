import sys
import string
import perceplearn
import os

def triplet(myInput,myOutput):
	
	for line in myInput:
		listWord=[]
		listTag=[]
		line=line.strip(" \n")
		line=line.strip("\n")
		features=line.split(' ')
		for i in features:
			
			listWord.append(i.split('/', 1)[0])
		#print(features)		
		for i in features:
			
			listTag.append(i.split('/', 1)[1])
		
		for a in range(0,len(listWord)):
			l={}
			if a==0:
				l["prev"]="BOS"
			else:
				l["prev"]=listWord[a-1].lower()
			l["curr"]=listWord[a].lower()
			if a==len(listWord)-1:
				l["next"]="EOS"
			else:
				l["next"]=listWord[a+1].lower()
			tagForTriplet=listTag[a]
			#print(listWord)
			myLine=tagForTriplet+" prev:"+l["prev"]+" curr:"+l["curr"]+" next:"+l["next"]+"\n"
			#print(myLine)		
			myOutput.write(myLine)

						
			a+=1			
	
def main():
	myInput=open(sys.argv[1],"r")   #train.pos file
	myOutput=open("trainTriplet","w")  #file in which triplets are written
	triplet(myInput,myOutput)
	output1=open("devTriplet","w")
	if len(sys.argv)>3:
		input1=open(sys.argv[4],"r")
		triplet(input1,output1)
		perceplearn.funct("trainTriplet",sys.argv[2],sys.argv[3],"devTriplet")
	else:
		perceplearn.funct("trainTriplet",sys.argv[2],0,0)	

if __name__=="__main__":
	main()
